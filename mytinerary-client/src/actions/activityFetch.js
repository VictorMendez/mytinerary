import axios from "axios";
import {
  GET_ACTIVITY,
  LOADACTIVITY,
  ACTIVITYERROR
} from "../actions/activityTypes";

export const getActivity = itineraryId => dispach => {
  dispach({
    type: LOADACTIVITY
  });

  axios
    .get(`http://localhost:5000/activities/${itineraryId}`)
    //.get(`http://192.168.0.21:5000/itinerary/${city}`)
    .then(data => {
      dispach({
        type: GET_ACTIVITY,
        payload: data.data
      });
    })
    .catch(err => {
      console.log("Error:", err);
      dispach({
        type: ACTIVITYERROR,
        payload: "Activity information not available"
      });
    });
};
