const express = require("express");
const app = express();
const morgan = require("morgan");
const path = require("path");
const { mongoose } = require("./database");
const cors = require("cors");
const port = process.env.PORT || 5000;
const passport = require("passport");

require("./config/passport")(passport);

app.use(morgan("dev"));
app.use(express.json());
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());
app.use("/api/cities", require("./routes/Cities"));
app.use("/itinerary", require("./routes/Itinerary"));
app.use("/activities", require("./routes/Activities"));
app.use("/auth/create", require("./routes/CreateUser"));
app.use("/auth/login", require("./routes/Login"));
app.use("/api/auth/google", require("./routes/Google"));

app.use(express.static(path.join(__dirname, "mytinerary-client", "public")));
app.listen(port);
console.log("Magic happens on port " + port);
