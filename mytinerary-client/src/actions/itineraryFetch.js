import axios from "axios";
import { GET_ITINERARYS, LOAD, ERR } from "./itineraryTypes";

export const getItinerarys = city => dispach => {
  dispach({
    type: LOAD
  });

  axios
    .get(`http://localhost:5000/itinerary/${city}`)
    //.get(`http://192.168.0.21:5000/itinerary/${city}`)
    .then(data => {
      dispach({
        type: GET_ITINERARYS,
        payload: data.data
      });
    })
    .catch(err => {
      console.log("Error:", err);
      dispach({
        type: ERR,
        payload: "Itinerarys information not available"
      });
    });
};
