const express = require("express");
const router = express.Router();
const ItinerarySchema = require("../models/Itinerary");

router.get("/", async (req, res) => {
  const Itinerarys = await ItinerarySchema.find();
  res.status(200).json(Itinerarys);
});
router.get("/:city", async (req, res) => {
  const city = req.params.city;
  const Itinerary = await ItinerarySchema.find({ city: city });
  res.status(200).json(Itinerary);
});

router.post("/", async (req, res) => {
  const itinerary = req.body;
  const newItinerary = await ItinerarySchema.create(itinerary);
  res.status(201).json(newItinerary);
});
router.delete("/:itineraryId", async (req, res) => {
  const { itineraryId } = req.params;
  const itineraryDelete = await ItinerarySchema.findByIdAndDelete(itineraryId);
  res.status(201).json(itineraryDelete);
});

router.put("/:itineraryId", async (req, res) => {
  const { itineraryId } = req.params;
  const { body: itinerary } = req;
  const itineraryUpdated = await ItinerarySchema.findByIdAndUpdate(
    itineraryId,
    itinerary
  );
  res.status(201).json(itineraryUpdated);
});

module.exports = router;
