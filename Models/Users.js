const mongoose = require("mongoose");
const { Schema } = mongoose;
const UserSchema = new Schema({
  googleId: { type: String },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  firstname: { type: String },
  lastname: { type: String },
  profilePic: { type: String },
  country: { type: String }
});
module.exports = mongoose.model("users", UserSchema);
