const JwtStrategy = require("passport-jwt").Strategy;
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const mongoose = require("mongoose");
const UserSchema = require("../models/Users");
const config = require("config");
const passport = require("passport");

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.get("JWTSecret");

module.exports = function(passport) {
  passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
      if (!jwt_payload.googleId) {
        const User = await UserSchema.findById(jwt_payload.id);
        if (User) return done(null, User);
        return done(null, false);
      } else {
        const User = await UserSchema.find({ googleId: jwt_payload.googleId });
        if (User) return done(null, User[0]);
        return done(null, false);
      }
    })
  );
};
