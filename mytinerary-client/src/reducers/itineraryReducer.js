import { GET_ITINERARYS, LOAD, ERR } from "../actions/itineraryTypes";

const initialState = {
  itinerarys: [],
  load: false,
  error: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ITINERARYS:
      return {
        ...state,
        itinerarys: action.payload,
        load: false,
        error: ""
      };
    case LOAD:
      return {
        ...state,
        load: true
      };
    case ERR:
      return {
        ...state,
        error: action.payload,
        load: false
      };
    default:
      return state;
  }
};
