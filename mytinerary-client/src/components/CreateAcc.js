import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { register } from "../actions/authActions";
import { clearErrors } from "../actions/errorActions";
class CreateAcc extends Component {
  constructor() {
    super();
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      profilePic: "http://cons.pe/img/avatar/generic-avatar.png",
      country: "",
      termsCheck: false,
      msg: null
    };
  }
  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    register: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired
  };

  componentDidUpdate(prevProps) {
    const { error } = this.props;
    if (error !== prevProps.error) {
      if (error.id === "REGISTER_FAIL") {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg: null });
      }
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onChangePic(e) {
    if (e.target.validity.valid === true && e.target.value.length !== 0) {
      this.setState({ [e.target.name]: e.target.value });
    } else {
      this.setState({
        [e.target.name]: "http://cons.pe/img/avatar/generic-avatar.png"
      });
    }
  }
  onSubmit = e => {
    e.preventDefault();
    const {
      firstname,
      lastname,
      email,
      password,
      profilePic,
      country
    } = this.state;
    if (this.state.termsCheck === true) {
      const newUser = {
        firstname,
        lastname,
        email,
        password,
        profilePic,
        country
      };

      this.props.register(newUser, this.props);
      this.props.clearErrors();
    } else {
      this.setState({ msg: "Accept Terms and Conditions" });
    }
  };
  checkBoxHandle() {
    const check = this.state.termsCheck;
    if (check === false) {
      this.setState({
        termsCheck: true
      });
    } else {
      this.setState({
        termsCheck: false
      });
    }
  }
  render() {
    return (
      <div>
        <div className="d-flex flex-column mt-3">
          <div>
            <h3 className="d-block text-center">Create Account</h3>
          </div>
          <div>
            <img
              src={this.state.profilePic}
              alt="profilePic"
              className="rounded-circle border border-dark w-25 h-25 d-block mx-auto"
            />
          </div>
          {this.state.msg ? (
            <div
              className="alert alert-danger w-75 d-block mx-auto text-center"
              role="alert"
            >
              {this.state.msg}
            </div>
          ) : null}
          <div className="mt-4 pr-5 pl-5">
            <form onSubmit={this.onSubmit}>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="firstname" className="mr-2 w-50">
                  First Name:
                </label>
                <input
                  type="text"
                  name="firstname"
                  value={this.state.firstname}
                  id="firstname"
                  className="form-control border border-dark rounded"
                  onChange={this.onChange.bind(this)}
                />
              </div>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="lastname" className="mr-2 w-50">
                  Last Name:
                </label>
                <input
                  type="text"
                  name="lastname"
                  value={this.state.lastname}
                  id="lastname"
                  className="form-control border border-dark rounded"
                  onChange={this.onChange.bind(this)}
                />
              </div>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="email" className="mr-2 w-50">
                  Email:
                </label>
                <input
                  type="email"
                  name="email"
                  value={this.state.email}
                  id="email"
                  className="form-control border border-dark rounded"
                  onChange={this.onChange.bind(this)}
                />
              </div>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="password" className="mr-2 w-50">
                  Password:
                </label>
                <input
                  type="password"
                  name="password"
                  value={this.state.password}
                  id="password"
                  className="form-control border border-dark rounded"
                  onChange={this.onChange.bind(this)}
                />
              </div>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="profilePic" className="mr-2 w-50">
                  Profile Picture:
                </label>
                <input
                  type="url"
                  name="profilePic"
                  value={this.state.profilePic}
                  id="profilePic"
                  className="form-control border border-dark rounded"
                  onChange={this.onChangePic.bind(this)}
                />
              </div>
              <div className="form-group d-row d-flex justify-content-around align-items-center">
                <label htmlFor="country" className="mr-2 w-50">
                  Country:
                </label>
                <select
                  name="country"
                  id="country"
                  value={this.state.country}
                  className="form-control border border-dark rounded"
                  onChange={this.onChange.bind(this)}
                >
                  <option value="Default">Select a Country...</option>
                  <option value="England">England</option>
                  <option value="France">France</option>
                  <option value="Germany">Germany</option>
                  <option value="Holland">Holland</option>
                  <option value="Ireland">Ireland</option>
                  <option value="Spain">Spain</option>
                  <option value="United States">United States</option>
                  <option value="Venezuela">Venezuela</option>
                </select>
              </div>
              <label className="small">
                <input
                  type="checkbox"
                  defaultChecked={this.state.termsCheck}
                  onClick={this.checkBoxHandle.bind(this)}
                />
                I agree to MYtinerary's{" "}
                <Link to="#">
                  <u>Terms and Conditions</u>
                </Link>
              </label>
              <button className="btn btn-danger d-block mx-auto">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(
  mapStateToProps,
  { register, clearErrors }
)(CreateAcc);
