const express = require("express");
const router = express.Router();
const ActivitiesSchema = require("../Models/Activities");

router.get("/", async (req, res) => {
  const Activities = await ActivitiesSchema.find();
  res.status(200).json(Activities);
});
router.get("/:itinerary", async (req, res) => {
  const itinerary = req.params.itinerary;
  const Activities = await ActivitiesSchema.find({
    itineraryId: itinerary
  });
  res.status(200).json(Activities);
});

router.post("/", async (req, res) => {
  const Activities = req.body;
  const newActivity = await ActivitiesSchema.create(Activities);
  res.status(201).json(newActivity);
});
router.delete("/:ActivityId", async (req, res) => {
  const { ActivityId } = req.params;
  const activityDelete = await ActivitiesSchema.findByIdAndDelete(ActivityId);
  res.status(201).json(activityDelete);
});

router.put("/:ActivityId", async (req, res) => {
  const { ActivityId } = req.params;
  const { body: activity } = req;
  const activityUpdated = await ActivitiesSchema.findByIdAndUpdate(
    ActivityId,
    activity
  );
  res.status(201).json(activityUpdated);
});

module.exports = router;
