import React, { Component } from "react";

class CityBanner extends Component {
  render() {
    if (this.props.city) {
      return (
        <div
          className="card text-white text-center d-flex"
          key={this.props.city.city}
        >
          <img
            src={this.props.city.img}
            className="card-img px-0 mx-0"
            style={{
              objectFit: "cover",
              width: "700px",
              maxWidth: "100%",
              maxHeight: "180px",
              filter: "brightness(50%)"
            }}
            alt={`Imagen de ${this.props.city.city}`}
          />

          <div className="card-img-overlay align-items-center d-flex justify-content-center">
            <h2 className="card-text text-white">
              {this.props.city.country} - {this.props.city.city}
            </h2>
          </div>
        </div>
      );
    } else {
      return false;
    }
  }
}
export default CityBanner;
