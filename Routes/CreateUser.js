const express = require("express");
const router = express.Router();
const UserSchema = require("../models/Users");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");

router.post("/", async (req, res) => {
  const {
    firstname,
    lastname,
    email,
    password,
    profilePic,
    country
  } = req.body;

  if (!firstname || !lastname || !email || !password) {
    return res.status(400).json({ msg: "Enter the Required Fields" });
  }

  const oldUser = await UserSchema.findOne({ email });
  if (oldUser) return res.status(400).json({ msg: "User Already Exist" });

  const newUser = new UserSchema({
    firstname,
    lastname,
    email,
    password,
    profilePic,
    country
  });

  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, async (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      await UserSchema.create(newUser);

      jwt.sign({ id: newUser.id }, config.get("JWTSecret"), (err, token) => {
        if (err) throw err;
        res.json({
          token,
          user: {
            firstname,
            lastname,
            email,
            profilePic,
            country
          }
        });
      });
    });
  });
});
module.exports = router;
