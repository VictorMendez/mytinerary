const mongoose = require("mongoose");
const { Schema } = mongoose;
const ActivitiesSchema = new Schema({
  itineraryId: { type: String },
  name: { type: String },
  activity_img: { type: String }
});
module.exports = mongoose.model("activities", ActivitiesSchema);
