import React, { Component } from "react";
import { Link } from "react-router-dom";
import Arrow from "../img/circled-right-2.png";
import CarouselImg from "./Carousel";
import Header from "./header";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// Lo Comentado Pertenece al Primer Diseño
class Index extends Component {
  constructor() {
    super();
    this.state = {
      msg: null
    };
  }
  static propTypes = {
    error: PropTypes.object.isRequired
  };
  componentDidMount(prevProps) {
    const { error } = this.props;
    if (error.id === "LOGOUT_SUCCESS") {
      this.setState({ msg: "Logout Succesfull" });
    } else {
      this.setState({ msg: null });
    }
  }
  render() {
    return (
      <div>
        <Header />
        <div className="text-center mt-4">
          {this.state.msg ? (
            <div
              className="alert alert-danger w-75 d-block mx-auto text-center"
              role="alert"
            >
              {this.state.msg}
            </div>
          ) : null}
          <p>
            <b>
              Find your perfect trip, designed by insiders who know and love
              their cities
            </b>
          </p>
          <h3>Start Browsing</h3>
          <Link to="/cities">
            <img className="w-25 pb-5 mt-3" src={Arrow} alt="Arrow" />
          </Link>
          {/* <p className="mt-3">Want to build your own MYtinerary</p> */}
          <CarouselImg />
        </div>
        {/* <div className="d-flex justify-content-around">
          <Link to="/login">Log in</Link>
          <Link to="/createAcc">Create Account</Link>
        </div> */}
      </div>
    );
  }
}
const mapStateToProps = (state, auth) => ({
  error: state.error,
  auth
});

export default connect(
  mapStateToProps,
  null
)(Index);
