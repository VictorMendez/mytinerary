import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { login } from "../actions/authActions";
import { clearErrors } from "../actions/errorActions";
import GoogleButton from "./GoogleButton";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      msg: null
    };
  }
  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired
  };

  componentDidUpdate(prevProps) {
    const { error } = this.props;
    if (error !== prevProps.error) {
      if (error.id === "LOGIN_FAIL") {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg: null });
      }
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit = e => {
    e.preventDefault();

    const { email, password } = this.state;
    const user = {
      email,
      password
    };
    this.props.login(user, this.props);
    this.props.clearErrors();
  };
  render() {
    return (
      <div className="d-flex flex-column mt-5">
        {this.state.msg ? (
          <div
            className="alert alert-danger w-75 d-block mx-auto text-center"
            role="alert"
          >
            {this.state.msg}
          </div>
        ) : null}
        <div>
          <h3 className="d-block text-center">Log In</h3>
        </div>
        <div className="mt-4 pr-5 pl-5">
          <form onSubmit={this.onSubmit}>
            <div className="form-group d-row d-flex justify-content-around align-items-center">
              <label htmlFor="email" className="mr-2 w-50">
                Email:
              </label>
              <input
                type="email"
                name="email"
                value={this.state.email}
                id="email"
                className="form-control border border-dark rounded"
                onChange={this.onChange.bind(this)}
              />
            </div>
            <div className="form-group d-row d-flex justify-content-around align-items-center">
              <label htmlFor="password" className="mr-2 w-50">
                Password:
              </label>
              <input
                type="password"
                name="password"
                value={this.state.password}
                id="password"
                className="form-control border border-dark rounded"
                onChange={this.onChange.bind(this)}
              />
            </div>
            <button className="btn btn-danger d-block mx-auto">Submit</button>
          </form>
          <GoogleButton />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(
  mapStateToProps,
  { login, clearErrors }
)(Login);
