const express = require("express");
const router = express.Router();
const UserSchema = require("../models/Users");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const passport = require("passport");

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const User = await UserSchema.findById(req.user.id);
    res.status(200).json(User);
  }
);

router.post("/", async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({ msg: "Enter the Required Fields" });
  }
  const User = await UserSchema.findOne({ email });
  if (!User) return res.status(400).json({ msg: "User Does Not Exist" });
  const isMatch = await bcrypt.compare(password, User.password);
  if (!isMatch) return res.status(400).json({ msg: "Invalid Password" });
  jwt.sign({ id: User.id }, config.get("JWTSecret"), (err, token) => {
    if (err) throw err;
    res.json({
      token,
      user: {
        name: User.firstname + " " + User.lastname,
        email: User.email,
        profilePic: User.profilePic
      }
    });
  });
});
module.exports = router;
