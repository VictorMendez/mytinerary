import {
  GET_ACTIVITY,
  LOADACTIVITY,
  ACTIVITYERROR
} from "../actions/activityTypes";

const initialState = {
  activities: [],
  load: false,
  error: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ACTIVITY:
      return {
        ...state,
        activities: action.payload,
        load: false,
        error: ""
      };
    case LOADACTIVITY:
      return {
        ...state,
        load: true
      };
    case ACTIVITYERROR:
      return {
        ...state,
        error: action.payload,
        load: false
      };
    default:
      return state;
  }
};
