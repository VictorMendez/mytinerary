const express = require("express");
const router = express.Router();
const CitySchema = require("../models/City");

router.get("/", async (req, res) => {
  const City = await CitySchema.find();
  res.status(200).json(City);
});

router.post("/", async (req, res) => {
  const city = req.body;
  const cityCreated = await CitySchema.create(city);
  res.status(201).json(cityCreated);
});

router.delete("/:cityId", async (req, res) => {
  const { cityId } = req.params;
  const cityDeleted = await CitySchema.findByIdAndDelete(cityId);
  res.status(201).json(cityDeleted);
});

router.put("/:cityId", async (req, res) => {
  const { cityId } = req.params;
  const { body: city } = req;
  const cityUpdated = await CitySchema.findByIdAndUpdate(cityId, city);
  res.status(201).json(cityUpdated);
});
module.exports = router;
