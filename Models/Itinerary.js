const mongoose = require("mongoose");
const { Schema } = mongoose;
const ItinerarySchema = new Schema({
  city: { type: String },
  title: { type: String, required: true },
  profilePic: { type: String, required: true },
  rating: { type: String },
  duration: { type: String },
  price: { type: String },
  hashtag: { type: Array }
});
module.exports = mongoose.model("itinerarys", ItinerarySchema);
